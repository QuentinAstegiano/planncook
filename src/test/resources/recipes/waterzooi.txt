Le waterzooï, une délicieuse spécialité d’origine belge, ici en version poisson.

Envie de goûter ? Y’a qu’à demander :-)

La recette complète du waterzooï de poisson, pommes de terre et julienne de légumes.

Préparation et cuisson : 35 minutes

Ingrédients pour 4 personnes
échalotes : 3
carottes : 300 gr
navets : 300 gr
poireaux : 300 gr
filets de merlu : 600 gr
crevettes : 200 gr
crème épaisse : 2 cs
beurre : 50 gr
fumet de poisson : 1 litre
safran : 1 dose
pommes de terre : 500 gr

Préparation de la recette :

Eplucher et laver les pommes de terre, les cuire à l’eau bouillante ou la vapeur pendant une vingtaine de minutes.

Tailler les échalotes, les navets, les carottes et les poireaux en julienne.
Dans une casserole, faire fondre le beurre, ajouter les légumes et laisser fondre doucement une dizaine de minutes.

Pendant ce temps, détailler le poisson en morceaux réguliers.
Faire frémir le fumet de poisson, y faire cuire les morceaux de poisson 5 minutes.
Retirer le poisson et réserver au chaud.
Verser la crème et le safran dans le fumet de poisson, amener à ébullition et faire réduire deux minutes.

Dans des cassolettes individuelles, ou des assiettes creuses, répartir les pommes de terre, les légumes, les morceaux de poisson et les crevettes.
Ajouter le fumet bien chaud et servir immédiatement.
