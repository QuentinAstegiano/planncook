package org.astegiano.planncook

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class IngredientParserTest {

    @Test
    fun `should parse ingredient from text`() {
        val text = "pommes de terre"
        val ingredient = IngredientParser.parse(text)

        assertThat(ingredient.name).isEqualTo("Pommes de terre")
    }
}

class QuantifiedIngredientParserTest {

    @Test
    fun `should parse quantified ingredient without quantity`() {
        val text = "pommes de terre"
        val ingredient = QuantifiedIngredientParser.parse(text)

        assertThat(ingredient.ingredient.name).isEqualTo("Pommes de terre")
        assertThat(ingredient.quantity).isEqualTo("")
    }

    @Test
    fun `should parse quantified ingredient with quantity and without unit`() {
        val text = "pommes de terre : 200"
        val ingredient = QuantifiedIngredientParser.parse(text)

        assertThat(ingredient.ingredient.name).isEqualTo("Pommes de terre")
        assertThat(ingredient.quantity).isEqualTo("200")
    }

    @Test
    fun `should parse quantified ingredient with non numerical quantity`() {
        val text = "sel : 1/2 cc"
        val ingredient = QuantifiedIngredientParser.parse(text)

        assertThat(ingredient.ingredient.name).isEqualTo("Sel")
        assertThat(ingredient.quantity).isEqualTo("1/2 cc")
    }

    @Test
    fun `should parse quantified ingredient with quantity and with unit`() {
        val text = "pommes de terre : 200 gr"
        val ingredient = QuantifiedIngredientParser.parse(text)

        assertThat(ingredient.ingredient.name).isEqualTo("Pommes de terre")
        assertThat(ingredient.quantity).isEqualTo("200 gr")
    }

    @Test
    fun `should parse multiple quantified ingredients`() {
        val text = "beurre\n" +
                "fumet de poisson : 1 litre\n" +
                "safran : 1\n" +
                "pommes de terre : 500 gr\n"

        val ingredients = QuantifiedIngredientParser.parseMultiple(text)
        assertThat(ingredients.size).isEqualTo(4)
        assertThat(ingredients[0].ingredient.name).isEqualTo("Beurre")
        assertThat(ingredients[1].ingredient.name).isEqualTo("Fumet de poisson")
        assertThat(ingredients[2].ingredient.name).isEqualTo("Safran")
        assertThat(ingredients[3].ingredient.name).isEqualTo("Pommes de terre")
    }
}

class IngredientsGroupParserTest {

    @Test
    fun `should parse ingredient group without name`() {
        val text = "beurre\n" +
                "fumet de poisson : 1 litre\n" +
                "safran : 1\n" +
                "pommes de terre : 500 gr\n"

        val group = IngredientsGroupParser.parse(text)
        assertThat(group.size).isEqualTo(1)
        assertThat(group[0].ingredients.size).isEqualTo(4)
        assertThat(group[0].name).isEqualTo("")
    }

    @Test
    fun `should parse ingredient group with name`() {
        val text = " - Partie 1 - \n" +
                "beurre\n" +
                "fumet de poisson : 1 litre\n" +
                "safran : 1\n" +
                "pommes de terre : 500 gr\n"

        val group = IngredientsGroupParser.parse(text)
        assertThat(group.size).isEqualTo(1)
        assertThat(group[0].ingredients.size).isEqualTo(4)
        assertThat(group[0].name).isEqualTo("Partie 1")
    }

    @Test
    fun `should not start a new group when an ingredient contains dash`() {
        val text = " - Partie 1 - \n" +
                "beurre\n" +
                "fumet de - poisson - : 1 litre\n" +
                "safran : 1\n" +
                "pommes de terre : 500 gr\n"

        val group = IngredientsGroupParser.parse(text)
        assertThat(group.size).isEqualTo(1)
        assertThat(group[0].ingredients.size).isEqualTo(4)
        assertThat(group[0].name).isEqualTo("Partie 1")
    }

    @Test
    fun `should parse multiple ingredient group with name`() {
        val text = "- pour la pâte - \n" +
                "farine de sarrasin : 330 gr\n" +
                "sel 5 gr\n" +
                "eau fraîche : 75 cl\n" +
                "- pour la garniture -\n" +
                "huile végétale : 1 cs\n" +
                "oeufs : 4\n" +
                "jambon : 100 gr\n"

        val group = IngredientsGroupParser.parse(text)
        assertThat(group.size).isEqualTo(2)
        assertThat(group[0].ingredients.size).isEqualTo(3)
        assertThat(group[0].name).isEqualTo("Pour la pâte")
        assertThat(group[1].ingredients.size).isEqualTo(3)
        assertThat(group[1].name).isEqualTo("Pour la garniture")
    }
}

class StepParserTest {

    @Test
    fun `should parse a single step`() {
        val text = "Eplucher et laver les pommes de terre"
        val step = StepParser.parse(text)

        assertThat(step.text).isEqualTo("Eplucher et laver les pommes de terre")
        assertThat(step.index).isEqualTo(1)
    }

    @Test
    fun `should parse multiple steps`() {
        val text = "Pendant ce temps, détailler le poisson en morceaux réguliers.\n" +
                " Faire frémir le fumet de poisson, y faire cuire les morceaux de poisson 5 minutes.\n" +
                " Retirer le poisson et réserver au chaud\n"
        val steps = StepParser.parseMultiple(text)

        assertThat(steps.size).isEqualTo(3)
        assertThat(steps[0].text).isEqualTo("Pendant ce temps, détailler le poisson en morceaux réguliers.")
        assertThat(steps[0].index).isEqualTo(1)
        assertThat(steps[1].text).isEqualTo("Faire frémir le fumet de poisson, y faire cuire les morceaux de poisson 5 minutes.")
        assertThat(steps[1].index).isEqualTo(2)
        assertThat(steps[2].text).isEqualTo("Retirer le poisson et réserver au chaud")
        assertThat(steps[2].index).isEqualTo(3)
    }

    @Test
    fun `should indentify special steps section group`() {
        val text = "Pendant ce temps, détailler le poisson en morceaux réguliers.\n" +
                " - pour la pâte -\n" +
                " Retirer le poisson et réserver au chaud\n"
        val steps = StepParser.parseMultiple(text)

        assertThat(steps.size).isEqualTo(3)
        assertThat(steps[0].special).isFalse()
        assertThat(steps[0].index).isEqualTo(1)
        assertThat(steps[1].special).isTrue()
        assertThat(steps[1].index).isEqualTo(0)
        assertThat(steps[1].text).isEqualTo("Pour la pâte")
        assertThat(steps[2].special).isFalse()
        assertThat(steps[2].index).isEqualTo(2)
    }
}

class AdditionnalInfosParserTest {

    @Test
    fun `should parse additional info`() {
        val text = "Le temps de repos de 2 heures de la pâte est un minimum.\n" +
            "Les galettes de sarrasin sont assez fragiles, attendez qu’elles soient bien cuites."

        val infos = AdditionnalInfoParser.parseMultiple(text)
        assertThat(infos.size).isEqualTo(2)
        assertThat(infos[0]).isEqualTo("Le temps de repos de 2 heures de la pâte est un minimum.")
        assertThat(infos[1]).isEqualTo("Les galettes de sarrasin sont assez fragiles, attendez qu’elles soient bien cuites.")

    }
}

class RecipeParserTest {

    @Test
    fun `should parse complete recipe`() {
        val text = "Le waterzooï, une délicieuse spécialité d’origine belge, ici en version poisson.\n" +
                "Envie de goûter ? Y’a qu’à demander :-)\n" +
                "Préparation et cuisson : 35 minutes\n" +
                "Ingrédients pour 4 personnes\n" +
                "échalotes : 3\n" +
                "carottes : 300 gr\n" +
                "Préparation de la recette :\n" +
                "Eplucher et laver les pommes de terre, les cuire à l’eau bouillante ou la vapeur pendant une vingtaine de minutes.\n" +
                "Tailler les échalotes, les navets, les carottes et les poireaux en julienne.\n" +
                "Bon à savoir:\n" +
                "Texte bon à savoir"

        val recipe = RecipeParser.parse(text)

        assertThat(recipe.intro.size).isEqualTo(2)
        assertThat(recipe.intro[0]).isEqualTo("Le waterzooï, une délicieuse spécialité d’origine belge, ici en version poisson.")
        assertThat(recipe.intro[1]).isEqualTo("Envie de goûter ? Y’a qu’à demander :-)")

        assertThat(recipe.time).isEqualTo("35 minutes")

        assertThat(recipe.ingredientsGroups.size).isEqualTo(1)
        assertThat(recipe.ingredientsGroups[0].ingredients.size).isEqualTo(2)
        assertThat(recipe.ingredientsGroups[0].ingredients[0].ingredient.name).isEqualTo("Échalotes")
        assertThat(recipe.ingredientsGroups[0].ingredients[1].ingredient.name).isEqualTo("Carottes")

        assertThat(recipe.steps.size).isEqualTo(2)
        assertThat(recipe.steps[0].text).isEqualTo("Eplucher et laver les pommes de terre, les cuire à l’eau bouillante ou la vapeur pendant une vingtaine de minutes.")
        assertThat(recipe.steps[1].text).isEqualTo("Tailler les échalotes, les navets, les carottes et les poireaux en julienne.")

        assertThat(recipe.additionalInfos.size).isEqualTo(1)
        assertThat(recipe.additionalInfos[0]).isEqualTo("Texte bon à savoir")
    }


    @Test
    fun `should parse complete recipe with groups`() {
        val text = "On connaît tous la galette de sarrasin comme spécialité bretonne. \n" +
                "Mais saviez-vous que c’est aussi un mets traditionnel québécois et acadien. Elle est surtout consommée sucrée au Québec, avec de la mélasse ou du sirop d'érable par exemple. \n" +
                "Préparation et cuisson : 20 minutes\n" +
                "Ingrédients pour 4 personnes\n" +
                "- pour la pâte -\n" +
                "farine de sarrasin : 330 gr\n" +
                "sel 5 gr\n" +
                "eau fraîche : 75 cl\n" +
                "- pour la garniture -\n" +
                "huile végétale : 1 cs\n" +
                "oeufs : 4\n" +
                "jambon : 100 gr:\n" +
                "Eplucher et laver les pommes de terre, les cuire à l’eau bouillante ou la vapeur pendant une vingtaine de minutes.\n" +
                "Tailler les échalotes, les navets, les carottes et les poireaux en julienne."

        val recipe = RecipeParser.parse(text)
        assertThat(recipe.ingredientsGroups.size).isEqualTo(2)
    }

}