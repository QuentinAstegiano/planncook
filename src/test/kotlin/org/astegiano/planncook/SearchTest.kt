package org.astegiano.planncook

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SearchTest {

    private fun mock(name: String, ingredients: List<String> = listOf()): Recipe {
        return Recipe(
                RecipeData(
                        intro = listOf(),
                        time = "",
                        ingredientsGroups = listOf(IngredientsGroup("", ingredients.map { QuantifiedIngredient(Ingredient(it)) })),
                        steps = listOf(),
                        additionalInfos = listOf()
                ),
                RecipeMetaData("id", name, "url")
        )
    }

    @Test
    fun `should find recipe when name match`() {
        val recipes = mock("recette de Poisson pané")
        val result = Search.match(listOf(recipes), "poisson")
        assertThat(result.size).isEqualTo(1)
    }

    @Test
    fun `should not find recipe when name does not match`() {
        val recipes = mock("recette de poisson pané")
        val result = Search.match(listOf(recipes), "viande")
        assertThat(result.size).isEqualTo(0)
    }

    @Test
    fun `should find recipe when ingredient match`() {
        val recipes = mock("recette de poisson pané", listOf("filet de hoki"))
        val result = Search.match(listOf(recipes), "hoki")
        assertThat(result.size).isEqualTo(1)
    }

    @Test
    fun `should not find recipe when ingredient does not match`() {
        val recipes = mock("recette de poisson pané", listOf("filet de hoki"))
        val result = Search.match(listOf(recipes), "boeuf")
        assertThat(result.size).isEqualTo(0)
    }
}
