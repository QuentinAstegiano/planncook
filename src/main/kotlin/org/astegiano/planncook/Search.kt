package org.astegiano.planncook

object Search {

    fun match(recipes: List<Recipe>, query: String) : List<Recipe> {
        val terms = getTerms(query)
        return recipes.filter { match(it, terms) }
    }

    private fun getTerms(query: String) : List<String> {
        return query.split(" ")
    }

    private fun match(recipe: Recipe, terms: List<String>) : Boolean {
        return terms.any { term ->
            recipe.meta.name.contains(term, ignoreCase = true) ||
            recipe.data.ingredientsGroups.any { group ->
                group.ingredients.any { (ingredient) ->
                    ingredient.name.contains(term, ignoreCase = true)
                }
            }
        }
    }
}
