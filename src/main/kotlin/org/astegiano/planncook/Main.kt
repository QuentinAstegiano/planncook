package org.astegiano.planncook

import com.fasterxml.jackson.databind.ObjectMapper
import freemarker.cache.ClassTemplateLoader
import freemarker.template.Configuration
import freemarker.template.ObjectWrapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.ModelAndView
import spark.Spark
import spark.template.freemarker.FreeMarkerEngine
import kotlin.reflect.companionObject


fun main(args: Array<String>) {
    if (args.isEmpty()) {
        throw IllegalArgumentException("Conf file path must be provided")
    }
    val conf = ConfReader().getFromFile(args[0])
    Main(conf).startServer()
}

class Main(val conf: Conf) {

    val log by logger()
    val engine by lazy {
        val conf = Configuration(Configuration.VERSION_2_3_23)
        conf.templateLoader = ClassTemplateLoader(ClassLoader.getSystemClassLoader(), "")
        FreeMarkerEngine(conf)
    }

    fun startServer() {
        Spark.port(conf.port)
        Spark.staticFiles.expireTime(60 * 60 * 24) // 24h de mise en cache
        Spark.staticFiles.location("/static")

        Spark.exception(Exception::class.java) { e, req, res ->
            log.error("Error while accessing '" + req.url() + " : " + e.message, e)
        }

        val youtube = Youtube(YoutubeClient(conf.youtube))

        Spark.get("/recettes", { req, res ->
            val model = hashMapOf<String, Any>("recipes" to youtube.allRecipes())
            engine.render(ModelAndView(model, "/templates/recipes.ftl"))
        })

        Spark.get("/recette/:id", { req, res ->
            val recipe = youtube.allRecipes().find { it.meta.id == req.params(":id") }
            if (recipe == null) {
                render404()
            } else {
                val model = hashMapOf<String, Any>("recipe" to recipe)
                engine.render(ModelAndView(model, "/templates/recipe.ftl"))
            }
        })

        Spark.get("/search", { req, res ->
            val query = req.queryParams("query")
            val matches = Search.match(youtube.allRecipes(), query)
                            .map { it.meta.id }

            ObjectMapper().writer().writeValueAsString(matches)
        })

        Spark.get("/topchronos", { req, res ->
            val model = hashMapOf<String, Any>("topchronos" to youtube.allTopChronos())
            engine.render(ModelAndView(model, "/templates/topchronos.ftl"))
        })

        Spark.get("/topchrono/:id", { req, res ->
            val topChrono = youtube.allTopChronos().find { it.meta.id == req.params(":id") }
            if (topChrono == null) {
                render404()
            } else {
                val model = hashMapOf<String, Any>("topchrono" to topChrono)
                engine.render(ModelAndView(model, "/templates/topchrono.ftl"))
            }
        })

        Spark.notFound( { req, res ->
            render404()
        })

    }

    private fun render404() = engine.render(ModelAndView(null, "/templates/error_404.ftl"))

}

fun <R : Any> R.logger(): Lazy<Logger> {
    return lazy { LoggerFactory.getLogger(unwrapCompanionClass(this.javaClass).name) }
}

fun <T : Any> unwrapCompanionClass(ofClass: Class<T>): Class<*> {
    return if (ofClass.enclosingClass != null && ofClass.enclosingClass.kotlin.companionObject?.java == ofClass) {
        ofClass.enclosingClass
    } else {
        ofClass
    }
}
