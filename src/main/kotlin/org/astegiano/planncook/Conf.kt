package org.astegiano.planncook

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.nio.file.FileSystems
import java.nio.file.Files

/**
 * Configuration definition & management.
 */
data class Conf(val externalBaseUrl: String,
                val port: Int,
                val youtube: YoutubeConf)

data class YoutubeConf(val key: String,
                       val playlists: YoutubePlaylistsConf)

data class YoutubePlaylistsConf(val recipes: String,
                                val topchrono: String)

class ConfReader {

    fun getFromFile(path: String): Conf {
        val mapper = ObjectMapper(YAMLFactory())
        mapper.registerModule(KotlinModule())

        return Files.newBufferedReader(FileSystems.getDefault().getPath(path)).use {
            mapper.readValue(it, Conf::class.java)
        }
    }
}


