package org.astegiano.planncook

import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.DateTime
import com.google.api.services.youtube.YouTube
import com.google.common.base.Supplier
import com.google.common.base.Suppliers
import java.util.concurrent.TimeUnit

class Youtube(client: YoutubeClient) {

    private val recipeCache: Supplier<List<Recipe>> =
            Suppliers.memoizeWithExpiration({
                client.allRecipes().map { RecipeConverter.convert(it) }
            }, 2, TimeUnit.HOURS)

    private val topChronoCache: Supplier<List<TopChrono>> =
            Suppliers.memoizeWithExpiration({
                client.allTopChrono().map { TopChronoConverter.convert(it) }
            }, 2, TimeUnit.HOURS)


    fun allRecipes() : List<Recipe> {
        return recipeCache.get()
    }

    fun  allTopChronos(): List<TopChrono> {
        return topChronoCache.get()
    }
}

class YoutubeClient(val conf: YoutubeConf) {

    private val youtube = YouTube.Builder(NetHttpTransport(), JacksonFactory(), null)
            .setApplicationName("plan-n-cook-web")
            .build()

    fun allRecipes(): List<YoutubeVideo> {
        return recipesChunk(conf.playlists.recipes)
    }

    fun allTopChrono(): List<YoutubeVideo> {
        return recipesChunk(conf.playlists.topchrono)
    }

    private fun recipesChunk(playlistId: String, pageToken: String = ""): List<YoutubeVideo> {
        val req = youtube.playlistItems().list("snippet")
        req.key = conf.key
        req.playlistId = playlistId
        req.maxResults = 50
        req.pageToken = pageToken

        val result = req.execute()
        val videos = result.items
                .filter {
                    it.snippet.title != "Private video"
                }
                .map {
                    val thumbnailUrl: String
                    if (it.snippet.thumbnails != null) {
                        if (it.snippet.thumbnails.maxres != null) {
                            thumbnailUrl = it.snippet.thumbnails.maxres.url
                        } else if (it.snippet.thumbnails.high != null) {
                            thumbnailUrl = it.snippet.thumbnails.high.url
                        } else if (it.snippet.thumbnails.medium != null) {
                            thumbnailUrl = it.snippet.thumbnails.medium.url
                        } else {
                            thumbnailUrl = ""
                        }
                    } else {
                        thumbnailUrl = ""
                    }
                    YoutubeVideo(it.snippet.resourceId.videoId,
                            it.snippet.title,
                            it.snippet.description,
                            it.snippet.publishedAt,
                            thumbnailUrl)
                }

        if (result.nextPageToken != null) {
            return videos + recipesChunk(playlistId, result.nextPageToken)
        } else {
            return videos
        }
    }
}

data class YoutubeVideo(val id: String,
                        val title: String,
                        val description: String,
                        val publication: DateTime,
                        val thumbnailUrl: String = "")
