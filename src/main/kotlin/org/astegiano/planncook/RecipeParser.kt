package org.astegiano.planncook

object RecipeConverter {

    fun convert(ytVideo: YoutubeVideo): Recipe {
        return Recipe(
                data = RecipeParser.parse(ytVideo.description),
                meta = RecipeMetaData(
                        id = ytVideo.id,
                        name = ytVideo.title,
                        thumbnailUrl = ytVideo.thumbnailUrl)
        )
    }
}

object TopChronoConverter {

    fun convert(ytVideo: YoutubeVideo): TopChrono {
        return TopChrono (
                data = TopChronoParser.parse(ytVideo.description),
                meta = TopChronoMetaData(
                        id = ytVideo.id,
                        name = ytVideo.title,
                        thumbnailUrl = ytVideo.thumbnailUrl)
        )
    }
}

object RecipeParser {

    fun parse(text: String): RecipeData {
        return parse(text.split("\n")
                .map { it.trim() }
                .filter { !it.isEmpty() })
    }

    fun parse(lines: List<String>): RecipeData {
        val intro = mutableListOf<String>()
        val ingredients = mutableListOf<String>()
        val steps = mutableListOf<String>()
        val additionalInfos = mutableListOf<String>()

        var ingredientsStart = false
        var stepsStart = false
        var additionalInfosStart = false
        var time = ""

        lines
                .forEach { line ->
                    if (line.startsWith("Préparation et cuisson", ignoreCase = true)) {
                        time = line.split(" : ")[1]
                    } else if (line.startsWith("Ingrédients", ignoreCase = true)) {
                        ingredientsStart = true
                    } else if (line.startsWith("Préparation", ignoreCase = true)) {
                        stepsStart = true
                    } else if (line.startsWith("Bon à savoir", ignoreCase = true)) {
                        additionalInfosStart = true
                    } else {
                        if (!ingredientsStart && !stepsStart && !additionalInfosStart) {
                            intro.add(line)
                        } else if (!stepsStart && !additionalInfosStart) {
                            ingredients.add(line)
                        } else if (!additionalInfosStart) {
                            steps.add(line)
                        } else {
                            additionalInfos.add(line)
                        }
                    }
                }

        return RecipeData(intro,
                time,
                IngredientsGroupParser.parse(ingredients),
                StepParser.parseMultiple(steps),
                AdditionnalInfoParser.parseMultiple(additionalInfos))
    }
}

object TopChronoParser {

    fun parse(text: String): TopChronoData {
        return parse(text.split("\n")
                .map { it.trim() }
                .filter { !it.isEmpty() })
    }

    fun parse(lines: List<String>): TopChronoData {
        val intro = mutableListOf<String>()
        val recipesIngredients = mutableMapOf<String, List<String>>()
        val currentIngredientsList = mutableListOf<String>()
        val steps = mutableListOf<String>()

        var currentRecipeName: String? = null
        var ingredientsStart = false
        var stepsStart = false

        lines
                .forEach { line ->
                    if (line.startsWith("Ingrédients", ignoreCase = true)) {
                        ingredientsStart = true
                        if (!currentIngredientsList.isEmpty()) {
                            recipesIngredients.put(currentRecipeName.orEmpty(), currentIngredientsList.toList())
                            currentIngredientsList.clear()
                        }
                        currentRecipeName = line
                                .replace("Ingrédients pour", "", ignoreCase = true)
                                .replace("Ingrédients", "", ignoreCase = true)
                                .trim()
                                .capitalize()
                    } else if (line.startsWith("Préparation", ignoreCase = true)) {
                        if (!currentIngredientsList.isEmpty()) {
                            recipesIngredients.put(currentRecipeName.orEmpty(), currentIngredientsList.toList())
                        }
                        stepsStart = true
                    } else {
                        if (!ingredientsStart && !stepsStart) {
                            intro.add(line)
                        } else if (!stepsStart) {
                            currentIngredientsList.add(line)
                        } else {
                            steps.add(line)
                        }
                    }
                }

        return TopChronoData(intro,
                recipesIngredients.map { TopChronoRecipeIngredientList(it.key, IngredientsGroupParser.parse(it.value)) },
                StepParser.parseMultiple(steps))
    }
}

object StepParser {

    fun parse(text: String, currentIndex: Int? = null): Step {
        val s = text.trim().capitalize()
        if (s.matches("^- .* -$".toRegex())) {
            return Step(
                    text = s.replace("- ", "").replace(" -", "").capitalize(),
                    special = true)
        } else {
            return Step(
                    text = s,
                    index = if (currentIndex == null) 1 else currentIndex + 1,
                    special = false)
        }
    }

    fun parseMultiple(text: String): List<Step> {
        return parseMultiple(text.split("\n"))
    }

    fun parseMultiple(lines: List<String>): List<Step> {
        var previousNonSpecialStep: Step? = null
        return lines
                .filter { !it.trim().isEmpty() }
                .map {
                    val thisStep = parse(it, previousNonSpecialStep?.index)
                    if (!thisStep.special) {
                        previousNonSpecialStep = thisStep
                    }

                    thisStep
                }
    }
}

object IngredientParser {

    fun parse(text: String): Ingredient {
        return Ingredient(text.trim().capitalize())
    }
}

object QuantifiedIngredientParser {

    fun parse(text: String): QuantifiedIngredient {
        val splited = text.split("\\h:\\h".toRegex())
        if (splited.size == 1) {
            return QuantifiedIngredient(IngredientParser.parse(text))
        }
        val ingredient = IngredientParser.parse(splited[0])
        return QuantifiedIngredient(ingredient, splited[1])
    }

    fun parseMultiple(text: String): List<QuantifiedIngredient> {
        return text
                .split("\n")
                .filter { !it.trim().isEmpty() }
                .map { parse(it) }
    }

    fun parseMultiple(lines: List<String>): List<QuantifiedIngredient> {
        return lines.filter { !it.trim().isEmpty() }
                .map { parse(it) }
    }
}

object IngredientsGroupParser {

    fun parse(text: String): List<IngredientsGroup> {
        return parse(text.split("\n")
                .map { it.trim() }
                .filter { !it.isEmpty() })
    }

    fun parse(lines: List<String>): List<IngredientsGroup> {
        if (lines.isEmpty()) {
            return listOf()
        }

        if (isTitle(lines[0])) {
            // La liste commence par un titre: on considére qu'elle est divisée en plusieurs groupes
            val list = mutableListOf<IngredientsGroup>()
            val thisGroup = mutableListOf<QuantifiedIngredient>()
            var thisTitle = extractTitle(lines[0])
            lines.drop(1).forEach {
                if (isTitle(it)) {
                    list.add(IngredientsGroup(thisTitle, thisGroup.toList()))
                    thisGroup.clear()
                    thisTitle = extractTitle(it)
                } else {
                    thisGroup.add(QuantifiedIngredientParser.parse(it))
                }
            }
            list.add(IngredientsGroup(thisTitle, thisGroup.toList()))
            return list
        } else {
            // La liste ne commence pas par un titre de groupe: on considére que c'est une liste "unique"
            return listOf(IngredientsGroup("", QuantifiedIngredientParser.parseMultiple(lines)))
        }
    }

    private fun isTitle(text: String): Boolean {
        return text.matches("^- .* -$".toRegex())
    }

    private fun extractTitle(text: String): String {
        return text.replace("- ", "")
                .replace(" -", "")
                .capitalize()
    }
}

object AdditionnalInfoParser {

    fun parse(text: String): String {
        return text.trim().capitalize()
    }

    fun parseMultiple(text: String): List<String> {
        return parseMultiple(text.split("\n"))
    }

    fun parseMultiple(lines: List<String>): List<String> {
        return lines
                .filter { !it.trim().isEmpty() }
                .map { parse(it) }
    }
}
