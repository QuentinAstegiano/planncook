package org.astegiano.planncook

/**
 * Recipe configuration & management
 */
data class Recipe(val data: RecipeData,
                  val meta: RecipeMetaData)

data class RecipeData(val intro: List<String>,
                  val time: String,
                  val ingredientsGroups: List<IngredientsGroup>,
                  val steps: List<Step>,
                  val additionalInfos: List<String>)

data class RecipeMetaData(val id: String,
                          val name: String,
                          val thumbnailUrl: String = "")

data class TopChrono(val data: TopChronoData,
                     val meta: TopChronoMetaData)

data class TopChronoData(val intro: List<String>,
                         val recipesIngredientsGroups: List<TopChronoRecipeIngredientList>,
                         val steps: List<Step>)

data class TopChronoRecipeIngredientList(val name: String,
                                         val ingredientsGroups: List<IngredientsGroup>)

data class TopChronoMetaData(val id: String,
                             val name: String,
                             val thumbnailUrl: String = "")

data class IngredientsGroup(val name: String,
                            val ingredients: List<QuantifiedIngredient>)

data class QuantifiedIngredient(val ingredient: Ingredient,
                                val quantity: String = "")

data class Ingredient(val name: String)

data class Step(val text: String,
                val index: Int = 0,
                val special: Boolean = false)

