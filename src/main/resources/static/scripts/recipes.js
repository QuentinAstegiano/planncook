$(function() {

    $("#search-input").keyup(function() {
        var query = $(this).val();
        $.getJSON("/search?query=" + query, function(json) {

            $(".marketing .container-recipe").each(function() {
                var id = $(this).attr("id");
                if ($.inArray(id, json) == -1) {
                    $(this).fadeOut();
                } else {
                    $(this).fadeIn();
                }
            });

            if (!$("#search-box").hasClass("search-fixed") || $("body").scrollTop() > 560) {
                $(document.body).animate({
                    "scrollTop":   $(".marketing").offset().top - 50
                }, 500);
            }

        });
    });

    $(window).on("scroll", function() {
        var shouldFix = $("body").scrollTop() > 560;
        if (shouldFix) {
            $("#search-box").toggleClass("search-fixed", true);
            $("#search-box-placeholder").show();
        } else {
            $("#search-box").toggleClass("search-fixed", false);
            $("#search-box-placeholder").hide();
        }
    });
});