$(function() {

    function resizeHeader() {
        $("#home-header").height($(window).height());
    }

    $( window ).resize(function() {
        resizeHeader();
    });

    resizeHeader();
});
