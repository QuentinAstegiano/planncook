<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${topchrono.meta.name}</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
    <link rel="stylesheet" href="/css/common.css"/>
    <link rel="stylesheet" href="/css/topchrono.css"/>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="jumbotron">
    <div class="container">
        <h1>${topchrono.meta.name}</h1>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 yt-container embed-responsive embed-responsive-16by9">
            <iframe style="margin: auto" src="https://www.youtube.com/embed/${topchrono.meta.id}?feature=oembed" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>

    <div class="row">
        <#if topchrono.data.recipesIngredientsGroups?has_content>
        <div class="col-md-4">
             <#list topchrono.data.recipesIngredientsGroups as recipe>
             <div class="panel panel-default panel-ingredients">
                <div class="panel-heading">
                    ${recipe.name}
                </div>
                <div class="panel-body">
                    <#list recipe.ingredientsGroups as group>
                        <#if group.name?has_content>
                            <h4>${group.name}</h4>
                        </#if>
                        <table class="table table-striped">
                            <#list group.ingredients as ing>
                            <tr>
                                <td>${ing.ingredient.name}</td>
                                <td>${ing.quantity}</td>
                            </tr>
                            </#list>
                        </table>
                    </#list>
                </div>
             </div>
             </#list>
        </div>
        </#if>
        <div class="<#if topchrono.data.recipesIngredientsGroups?has_content>col-md-8<#else>col-md-12</#if>">
           <div class="panel panel-default panel-steps">
                <div class="panel-heading">
                    Le programme
                </div>
                <div class="panel-body">
                    <#list topchrono.data.steps as step>
                        <p <#if step?is_last>class="last-step"</#if>>${step.text}</p>
                    </#list>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>
