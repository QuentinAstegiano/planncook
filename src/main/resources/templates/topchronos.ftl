<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plan'N'Cook - Alpha version</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
    <link rel="stylesheet" href="/css/common.css"/>
    <link rel="stylesheet" href="/css/topchronos.css"/>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="jumbotron">
    <div class="container">
        <h1>Les Session Top Chrono</h1>
        <p>
            &Ccedil;a vous dirait de pr&eacute;parer en un temps record une semaine de bons petits plats ?
        </p>
        <p>
            On s&eacute;lectionne 3 plats dans le menu de la semaine, et on vous montre comment les pr&eacute;parer en simultan&eacute;.
            On multiplie les quantit&eacute;s par 2, et hop en moins de 2 heures Top Chrono, voil&agrave; 6 plats pr&ecirc;ts &agrave; &ecirc;tre r&eacute;chauff&eacute;s tout au long de la semaine !
        </p>
    </div>
</div>

<div class="container marketing">

    <#list topchronos as topchrono>

        <div class="container-recipe" id="${topchrono.meta.id}">

            <#if !topchrono?is_first>
            <hr class="featurette-divider">
            </#if>

            <div class="row featurette">
                <div class="col-md-7 <#if topchrono?is_even_item>col-md-push-5</#if>">
                    <h2 class="featurette-heading">${topchrono.meta.name}</h2>
                    <#if topchrono.data.recipesIngredientsGroups ?has_content>
                        <p class="lead">Pr&eacute;parer en un temps record les recettes de la semaine:
                            <ul>
                            <#list topchrono.data.recipesIngredientsGroups as recipe>
                                <li class="lead"><strong>${recipe.name}</strong></li>
                            </#list>
                            </ul>
                        </p>
                    <#else>
                        <p class="lead">
                        <#list topchrono.data.intro as line>
                            ${line}
                        </#list>
                        </p>
                    </#if>
                    <p>
                        <a class="btn btn-primary btn-lg" href="/topchrono/${topchrono.meta.id}" role="button">Voir le programme !</a>
                    </p>
                </div>
                <div class="col-md-5 <#if topchrono?is_even_item>col-md-pull-7</#if>">
                    <#if topchrono.meta.thumbnailUrl?has_content>
                        <a href="/topchrono/${topchrono.meta.id}">
                            <img class="featurette-image img-responsive center-block" src="${topchrono.meta.thumbnailUrl}" />
                         </a>
                     </#if>
                </div>
            </div>

        </div>

    </#list>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>
</html>
