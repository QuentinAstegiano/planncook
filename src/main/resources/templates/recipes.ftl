<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plan'N'Cook - Alpha version</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
    <link rel="stylesheet" href="/css/common.css"/>
    <link rel="stylesheet" href="/css/recipes.css"/>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

   <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <#list recipes?chunk(5)[0] as recipe>
        <div class="item <#if recipe?is_first>active</#if>">
          <div class="container" style="background-image: url('${recipe.meta.thumbnailUrl}')">
            <div class="carousel-caption">
              <h2>${recipe.meta.name}</h2>
              <p><a class="btn btn-lg btn-primary" href="/recette/${recipe.meta.id}" role="button">D&eacute;couvrir la recette</a></p>
            </div>
          </div>
        </div>
        </#list>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Pr&eacute;c&eacute;dent</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Suivant</span>
      </a>
    </div><!-- /.carousel -->

<div class="container marketing">

    <div class="row search-box" id="search-box-placeholder" style="display: none">
        <div class="col-md-12">
        </div>
    </div>
    <div class="row search-box" id="search-box">
        <div class="col-md-12">
            <input type="text" id="search-input" class="form-control" placeholder="Une recherche ? Entrez un ingr&eacute;dient, une id&eacute;e, ... " />
        </div>
    </div>

    <#list recipes as recipe>

        <div class="container-recipe" id="${recipe.meta.id}">

            <#if !recipe?is_first>
            <hr class="featurette-divider">
            </#if>

            <div class="row featurette">
                <div class="col-md-12 featurette-header" style="background-image: url('${recipe.meta.thumbnailUrl}')">
                    <div class="featurette-header-text">
                        <h2 class="featurette-heading">${recipe.meta.name}</h2>
                        <p>
                            <a class="btn btn-primary btn-lg" href="/recette/${recipe.meta.id}" role="button">Voir la recette !</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="lead">
                    <#list recipe.data.intro as line>
                        ${line}
                        <#if !line?is_last><br/></#if>
                    </#list>
                    </p>
                </div>

            </div>

        </div>

    </#list>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="/scripts/recipes.js"></script>

</body>
</html>
