#!/bin/sh

version=$1

gradle shadowJar
scp build/dist/plan-n-cook-$version.jar plan-n-cook-server:~
ssh plan-n-cook-server "sudo /sbin/service plan-n-cook stop && rm /home/ec2-user/current-plan-n-cook"
ssh plan-n-cook-server "ln -s /home/ec2-user/plan-n-cook-$version.jar /home/ec2-user/current-plan-n-cook && sudo /sbin/service plan-n-cook start"

